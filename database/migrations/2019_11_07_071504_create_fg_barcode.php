<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFgBarcode extends Migration
{
    protected $connection = 'sqlsrv';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('fg_barcode', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('DI_KEY');
            $table->string('fg_code',30);
            $table->string('DI_REF',11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fg_barcode');
    }
}
