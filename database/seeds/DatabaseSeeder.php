<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\SETABLE::class, 5000)->create();
        factory(App\SOTABLE::class, 5000)->create();
    }
}
