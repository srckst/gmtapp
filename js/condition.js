$('.brandline1').each(function(){
    var newText = $(this).text();

    if (newText == '00') {
        $(this).text(newText + ' ' + '-' + ' ' + 'แผ่นเปลือย');
    }
    else if (newText == '01') {
        $(this).text(newText + '' + '-' + ' ' + 'VIP (มอก.)');
    }
    else if (newText == '04') {
        $(this).text(newText + '' + '-' + ' ' + 'GM');
    }
    else if (newText == '05') {
        $(this).text(newText + '' + '-' + ' ' + 'SHOGUN');
    }
    else if (newText == '06') {
        $(this).text(newText + '' + '-' + ' ' + 'เพชร5ดาว');
    }
    else if (newText == '09') {
        $(this).text(newText + '' + '-' + ' ' + 'เกรดC');
    }
    else if (newText == '10') {
        $(this).text(newText + '' + '-' + ' ' + 'ต้นไม้');
    }
    else if (newText == '11') {
        $(this).text(newText + '' + '-' + ' ' + 'SCL');
    }
    else if (newText == '12') {
        $(this).text(newText + '' + '-' + ' ' + 'RPG');
    }
    else if (newText == '13') {
        $(this).text(newText + ' ' + '-' + ' ' + 'SUPER');
    }
    else if (newText == '14') {
        $(this).text(newText + ' ' + '-' + ' ' + 'NOLOGO');
    }
    else if (newText == '15') {
        $(this).text(newText + ' ' + '-' + ' ' + 'ตราเพชร');
    }
    else if (newText == '16') {
        $(this).text(newText + ' ' + '-' + ' ' + 'ST');
    }
    else if (newText == '17') {
        $(this).text(newText + ' ' + '-' + ' ' + '3G');
    }
    else if (newText == '18') {
        $(this).text(newText + ' ' + '-' + ' ' + 'Maxum');
    }
    else if (newText == '19') {
        $(this).text(newText + '' + '-' + ' ' + 'City bord');
    }
    else if (newText == '20') {
        $(this).text(newText + ' ' + '-' + ' ' + 'สีธรรมดา');
    }
    else if (newText == '21') {
        $(this).text(newText + ' ' + '-' + ' ' + 'ขาวผ่อง');
    }
    else if (newText == '22') {
        $(this).text(newText + ' ' + '-' + ' ' + 'อดามาส');
    }
    else if (newText == '23') {
        $(this).text(newText + ' ' + '-' + ' ' + 'นาเดีย');
    }
    else if (newText == '24') {
        $(this).text(newText + ' ' + '-' + ' ' + 'โชกุน');
    }
    else if (newText == '25') {
        $(this).text(newText + ' ' + '-' + ' ' + 'บิ๊ก-บอย');
    }
    else if (newText == '26') {
        $(this).text(newText + ' ' + '-' + ' ' + 'ยิปแม๊ก');
    }
    else if (newText == '27') {
        $(this).text(newText + ' ' + '-' + ' ' + 'TRUSUS');
    }
    else if (newText == '28') {
        $(this).text(newText + ' ' + '-' + ' ' + 'OPSKY');
    }
    else if (newText == '29') {
        $(this).text(newText + ' ' + '-' + ' ' + 'DD bord');
    }
    else if (newText == '30') {
        $(this).text(newText + ' ' + '-' + ' ' + 'CC');
    }
    else if (newText == '31') {
        $(this).text(newText + ' ' + '-' + ' ' + 'GM กัมพูชา');
    }
    else if (newText == '32') {
        $(this).text(newText + ' ' + '-' + ' ' + 'TOA');
    }
    else if (newText == '33') {
        $(this).text(newText + ' ' + '-' + ' ' + 'HOFF GM');
    }
    else if (newText == '34') {
        $(this).text(newText + ' ' + '-' + ' ' + 'DIC');
    }
    else if (newText == '94') {
        $(this).text(newText + ' ' + '-' + ' ' + 'รอติดFOIL');
    }
    else if (newText == '9W') {
        $(this).text(newText + ' ' + '-' + ' ' + 'รอติดWAX');
    }
    else if (newText == '99') {
        $(this).text(newText + ' ' + '-' + ' ' + 'แผ่นรอคัด');
    }
    else if (newText == '44') {
        $(this).text(newText + ' ' + '-' + ' ' + 'GM เบาๆ');
    }
    else if (newText == '9H') {
        $(this).text(newText + ' ' + '-' + ' ' + 'รอเคลือบ WAX เจาะรูคู');
    }
    else if (newText == '9O') {
        $(this).text(newText + ' ' + '-' + ' ' + 'รอเคลือบ WAX เจาะรูเดี่ยว');
    }
    else {
        $(this).text('ไม่พบข้อมูลแบรนด์');
    }

});

$(document).ready(function(){

    $('.outfg3').each(function(){
        var itemNo = $(this).text().substr(4,7);
        var brand = $(this).text().substr(24,22);
        var data = $(this).text('B1'+ brand + itemNo);
        console.log(data.text());
        if (data.text() == 'B100-A10109') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B100-A10112') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.4mx12mm');
        }
        else if (data.text() == 'B100-A10116') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.4mx15mm');
        }
        else if (data.text() == 'B100-A10209') {
            $(this).text('ยิปซั่ม ขอบลาด 1.22mx2.44mx9mm');
        }
        else if (data.text() == 'B100-A10909') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.2mx9mm');
        }
        else if (data.text() == 'B100-A10912') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.2mx12mm');
        }
        else if (data.text() == 'B100-A30209') {
            $(this).text('ยิปซั่ม ขอบลาด ทนชื้น 1.22mx2.44mx9mm');
        }
        else if (data.text() == 'B100-A30212') {
            $(this).text('ยิปซั่ม ขอบลาด ทนชื้น 1.22mx2.44mx12mm');
        }
        else if (data.text() == 'B100-A60116') {
            $(this).text('ยิปซั่ม ขอบลาด ทนไฟ 1.2mx2.4mx15mm');
        }
        else if (data.text() == 'B100-B10109') {
            $(this).text('ยิปซั่ม - ขอบเรียบ  1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B100-B10112') {
            $(this).text('ยิปซั่ม - ขอบเรียบ  1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B100-B30109') {
            $(this).text('ยิปซั่ม ขอบเรียบ ทนชื้น 1.2x2.4mx9mm');
        }
        else if (data.text() == 'B101-AZ0109') {
            $(this).text('ยิปซั่ม VIP ขอบลาด 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B101-A10112') {
            $(this).text('ยิปซั่ม VIP ขอบลาด (มอก.) 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B101-A30109') {
            $(this).text('ยิปซั่ม VIP ขอบลาด ทนชื้น 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B101-A10109') {
            $(this).text('ยิปซั่ม VIP ขอบลาด (มอก.) 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B101-B10109') {
            $(this).text('ยิปซั่ม VIP ขอบเรียบ (มอก.) 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-A10109') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-A10112') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B104-A10209') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-A10212') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.22x2.44m.x12mm');
        }
        else if (data.text() == 'B104-A30109') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-A30112') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B104-A30209') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-A40109') {
            $(this).text('ยิปซั่ม GM ขอบลาด กันร้อน 1.2x 2.4m.x9mm');
        }
        else if (data.text() == 'B104-A40209') {
            $(this).text('ยิปซั่ม GM ขอบลาด กันร้อน 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-A40309') {
            $(this).text('ยิปซั่ม GM ขอบลาด กันร้อน 1.21x2.42m.x9mm');
        }
        else if (data.text() == 'B104-A70109') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-AW0109') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น WAX 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-AW0209') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น WAX 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-B10109') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-B10112') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B104-B10209') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-B10212') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.22x2.44mx12mm');
        }
        else if (data.text() == 'B104-B10309') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.21x2.42cmx9mm');
        }
        else if (data.text() == 'B104-B30109') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ ทนชื้น 1.2 x 2.4 m.x9 mm');
        }
        else if (data.text() == 'B104-B30209') {
            $(this).text('ยิปซั่ม GMขอบเรียบ ทนชื้น 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-B30309') {
            $(this).text('ยิปซั่ม GM ขอบเรียบทนชื้น1.21x2.42cmx9mm');
        }
        else if (data.text() == 'B104-B40109') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B104-B40209') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ กันร้อน1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B104-B40309') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ กันร้อน 1.21x2.42mx9mm');
        }
        else if (data.text() == 'B105-A10109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B105-A30109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B105-A40109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบลาด กันร้อน1.2x2.4m.x 9');
        }
        else if (data.text() == 'B105-B10109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบเรียบ  1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B106-A10109') {
            $(this).text('ยิปซั่ม เพชร5ดาว ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B106-A30109') {
            $(this).text('ยิปซั่ม เพชรห้าดาว ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B106-A30709') {
            $(this).text('ยิปซั่ม เพชรห้าดาว ขอบลาด ทนชื้น 1.0x2.4m.x9mm');
        }
        else if (data.text() == 'B106-B10109') {
            $(this).text('ยิปซั่ม เพชร5ดาว ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B106-B10209') {
            $(this).text('ยิปซั่ม เพชร5ดาว ขอบเรียบ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B109-A10109') {
            $(this).text('เกรด Cขอบลาด1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B109-A10112') {
            $(this).text('เกรดC ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B109-A30109') {
            $(this).text('เกรดC ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B109-A40109') {
            $(this).text('เกรดC ขอบลาด กันร้อน 1.2 x 2.4 m.x 9 ');
        }
        else if (data.text() == 'B110-A10109') {
            $(this).text('ยิปซั่ม ต้นไม้ ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B110-A30109') {
            $(this).text('ยิปซั่มต้นไม้ ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B110-A40109') {
            $(this).text('ยิปซั่ม ต้นไม้ ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B111-A10109') {
            $(this).text('ยิปซั่ม SCL ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B111-A10112') {
            $(this).text('ยิปซั่ม SCL ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B111-A30109') {
            $(this).text('ยิปซั่ม SCL ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B111-A40109') {
            $(this).text('ยิปซั่ม SCL ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B112-A10109') {
            $(this).text('ยิปซั่ม RPG ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B112-A30109') {
            $(this).text('ยิปซั่ม RPG ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B112-A40109') {
            $(this).text('ยิปซั่ม RPG ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B113-A10109') {
            $(this).text('ยิปซั่ม SUPER ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B113-A30109') {
            $(this).text('ยิปซั่ม SUPER ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B113-A40109') {
            $(this).text('ยิปซั่ม SUPER ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B114-A10109') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B114-A10112') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B114-A10512') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x3m.x12mm');
        }
        else if (data.text() == 'B114-A10812') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x2.7m.x12mm');
        }
        else if (data.text() == 'B114-A30109') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B114-A30112') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B114-A30512') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x3m.x12mm');
        }
        else if (data.text() == 'B114-A30615') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x2.5m.x9mm');
        }
        else if (data.text() == 'B114-B10109') {
            $(this).text('ยิปซั่ม NOLOGO ขอบเรียบ 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B114-B10112') {
            $(this).text('ยิปซั่ม NOLOGO ขอบเรียบ 1.2mx2.4mx12mm');
        }
        else if (data.text() == 'B114-B10712') {
            $(this).text('ยิปซั่ม NOLOGO ขอบเรียบ 1.2mx2.3mx12mm');
        }
        else if (data.text() == 'B115-A10109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B115-A30109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B115-A40109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B115-B10109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B116-A10109') {
            $(this).text('ยิปซั่ม ST ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B116-A30109') {
            $(this).text('ยิปซั่ม ST ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B116-A40109') {
            $(this).text('ยิปซั่ม ST ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B117-A30109') {
            $(this).text('ยิปซั่ม 3G ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B118-A10109') {
            $(this).text('ยิปซั่ม Maxum ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B118-A30109') {
            $(this).text('ยิปซั่ม Maxum ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B118-A40109') {
            $(this).text('ยิปซั่ม Maxum ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B118-A90109') {
            $(this).text('ยิปซั่ม Maxum.N ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B119-A10109') {
            $(this).text('ยิปซั่ม City bord ขบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B119-A30109') {
            $(this).text('ยิปซั่ม City bord ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B119-A40109') {
            $(this).text('ยิปซั่ม City bord ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B125-A10109') {
            $(this).text('ยิปซั่ม บิ๊ก-บอย ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B125-A30109') {
            $(this).text('ยิปซั่ม บิ๊ก-บอย ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B125-A40109') {
            $(this).text('ยิปซั่ม บิ๊ก-บอย ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B126-A10209') {
            $(this).text('ยิปซั่ม ยิปแม๊ก ขอบลาด 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B127-A10195') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด 1.2x2.4m.x9.5');
        }
        else if (data.text() == 'B127-A10595') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด1.2x3.0mx9.5mm');
        }
        else if (data.text() == 'B127-A10895') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด 1.2x2.7m.x9.5');
        }
        else if (data.text() == 'B127-A30195') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด ทนชื้น 1.2x2.4m.x9.5');
        }
        else if (data.text() == 'B127-B10209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B127-B10412') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ 1.22x3.05x12mm');
        }
        else if (data.text() == 'B127-B30209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ ทนชื้น 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B127-B30412') {
            $(this).text('ยิปซั่มTRUSUS ขอบเรียบ ทนชื้น 1.22x3.05x12mm');
        }
        else if (data.text() == 'B127-B40209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ กันร้อน1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B127-B60209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ ทนไฟ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B128-A10595') {
            $(this).text('ยิปซั่ม OPSKY ขอบลาด 1.2x3.0m9.5mm');
        }
        else if (data.text() == 'B129-A10109') {
            $(this).text('ยิปซั่ม ขอบลาด ตรา DD board 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B129-B10109') {
            $(this).text('ยิปซั่ม DD bord ขอบเรียบ 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B131-A10109') {
            $(this).text('ยิปซั่ม GM กัมพูชา ขอบลาด 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B132-A10109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B132-A10112') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B132-A10209') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B132-A30109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B132-A40109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B132-AW0209') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด ทนชื้น WAX 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B132-AW0212') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด ทนชื้น WAX 1.22x2.44m.x12mm');
        }
        else if (data.text() == 'B132-B10109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B132-B10112') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบเรียบ 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B132-B30109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบเรียบ ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B133-A10109') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B133-A10112') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B133-A10512') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x3m.x12mm');
        }
        else if (data.text() == 'B133-A10709') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.3m.x9mm');
        }
        else if (data.text() == 'B133-A10712') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.3m.x12mm');
        }
        else if (data.text() == 'B133-A10812') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.7m.x12mm');
        }
        else if (data.text() == 'B133-A30112') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด ทนชื้น 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B133-A30512') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด ทนชิ้น 1.2x3m.x12mm');
        }
        else if (data.text() == 'B133-A30812') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด ทนชิ้น 1.2x2.7m.x12mm');
        }
        else if (data.text() == 'B134-A10109') {
            $(this).text('ยิปซั่ม DIC ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B200-A10109') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B200-A10112') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.4mx12mm');
        }
        else if (data.text() == 'B200-A10116') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.4mx15mm');
        }
        else if (data.text() == 'B200-A10209') {
            $(this).text('ยิปซั่ม ขอบลาด 1.22mx2.44mx9mm');
        }
        else if (data.text() == 'B200-A10909') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.2mx9mm');
        }
        else if (data.text() == 'B200-A10912') {
            $(this).text('ยิปซั่ม ขอบลาด 1.2mx2.2mx12mm');
        }
        else if (data.text() == 'B200-A30209') {
            $(this).text('ยิปซั่ม ขอบลาด ทนชื้น 1.22mx2.44mx9mm');
        }
        else if (data.text() == 'B200-A30212') {
            $(this).text('ยิปซั่ม ขอบลาด ทนชื้น 1.22mx2.44mx12mm');
        }
        else if (data.text() == 'B200-A60116') {
            $(this).text('ยิปซั่ม ขอบลาด ทนไฟ 1.2mx2.4mx15mm');
        }
        else if (data.text() == 'B200-B10109') {
            $(this).text('ยิปซั่ม - ขอบเรียบ  1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B200-B10112') {
            $(this).text('ยิปซั่ม - ขอบเรียบ  1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B200-B30109') {
            $(this).text('ยิปซั่ม ขอบเรียบ ทนชื้น 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B201-A10112') {
            $(this).text('ยิปซั่ม VIP ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B201-A30109') {
            $(this).text('ยิปซั่ม VIP ขอบลาด ทนชื้น 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B201-B10109') {
            $(this).text('ยิปซั่ม VIP ขอบเรียบ (มอก.) 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-A10109') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-A10112') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B204-A10209') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-A10212') {
            $(this).text('ยิปซั่ม GM ขอบลาด 1.22x2.44m.x12mm');
        }
        else if (data.text() == 'B204-A30112') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B204-A30209') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-A40109') {
            $(this).text('ยิปซั่ม GM ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-A40209') {
            $(this).text('ยิปซั่ม GM ขอบลาด กันร้อน 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-A40309') {
            $(this).text('ยิปซั่ม GM ขอบลาด กันร้อน 1.21x2.42m.x9mm');
        }
        else if (data.text() == 'B204-A70109') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-AW0109') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น WAX 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-AW0209') {
            $(this).text('ยิปซั่ม GM ขอบลาด ทนชื้น WAX 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-B10109') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-B10112') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B204-B10209') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-B10212') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.22x2.44mx12mm');
        }
        else if (data.text() == 'B204-B10309') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ 1.21x2.42cmx9mm');
        }
        else if (data.text() == 'B204-B30109') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-B30209') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ ทนชื้น 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-B30309') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ ทนชื้น1.21x2.42cmx9mm');
        }
        else if (data.text() == 'B204-B40109') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B204-B40209') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ กันร้อน1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B204-B40309') {
            $(this).text('ยิปซั่ม GM ขอบเรียบ กันร้อน1.21x2.42cmx9mm');
        }
        else if (data.text() == 'B205-A10109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบลาด1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B205-A30109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B205-A40109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B205-B10109') {
            $(this).text('ยิปซั่ม SHOGUN ขอบเรียบ  1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B206-A10109') {
            $(this).text('ยิปซั่ม เพชร5ดาว ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B206-A30109') {
            $(this).text('ยิปซั่ม เพชรห้าดาว ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B206-A30709') {
            $(this).text('ยิปซั่ม เพชรห้าดาว ขอบลาด ทนชื้น 1.0x2.4m.x9mm');
        }
        else if (data.text() == 'B206-B10109') {
            $(this).text('ยิปซั่ม เพชร5ดาว ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B206-B10209') {
            $(this).text('ยิปซั่ม เพชร5ดาว ขอบเรียบ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B209-A10109') {
            $(this).text('เกรด C ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B209-A10112') {
            $(this).text('เกรด Cขอบลาด1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B209-A30109') {
            $(this).text('เกรดCขอบลาดทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B209-A40109') {
            $(this).text('เกรดCขอบลาดกันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B210-A10109') {
            $(this).text('ยิปซั่ม ต้นไม้ ขอบลาด1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B210-A30109') {
            $(this).text('ยิปซั่ม ต้นไม้ ขอบลาดทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B210-A40109') {
            $(this).text('ยิปซั่ม ต้นไม้ ขอบลาดกันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B211-A10109') {
            $(this).text('ยิปซั่ม SCLขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B211-A10112') {
            $(this).text('ยิปซั่ม SCL ขอบลาด1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B211-A30109') {
            $(this).text('ยิปซั่ม SCL ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B211-A40109') {
            $(this).text('ยิปซั่ม SCL ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B212-A10109') {
            $(this).text('ยิปซั่ม RPG ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B212-A30109') {
            $(this).text('ยิปซั่ม RPG ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B212-A40109') {
            $(this).text('ยิปซั่ม RPG ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B213-A10109') {
            $(this).text('ยิปซั่ม SUPER ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B213-A30109') {
            $(this).text('ยิปซั่ม SUPER ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B213-A40109') {
            $(this).text('ยิปซั่ม SUPER ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B214-A10109') {
            $(this).text('ยิปซั่ม-ขอบลาด1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B214-A10112') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B214-A10512') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x3m.x12mm');
        }
        else if (data.text() == 'B214-A10812') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด 1.2x2.7m.x12mm');
        }
        else if (data.text() == 'B214-A30109') {
            $(this).text('ยิปซั่ม- ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B214-A30112') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B214-A30512') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x3m.x12mm');
        }
        else if (data.text() == 'B214-A30615') {
            $(this).text('ยิปซั่ม NOLOGO ขอบลาด ทนชื้น 1.2x2.5m.x9mm');
        }
        else if (data.text() == 'B214-B10109') {
            $(this).text('ยิปซั่ม NOLOGO ขอบเรียบ 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B214-B10112') {
            $(this).text('ยิปซั่ม NOLOGO ขอบเรียบ 1.2mx2.4mx12mm');
        }
        else if (data.text() == 'B214-B10712') {
            $(this).text('ยิปซั่ม NOLOGO ขอบเรียบ 1.2mx2.3mx12mm');
        }
        else if (data.text() == 'B215-A10109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B215-A30109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B215-A40109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B215-B10109') {
            $(this).text('ยิปซั่ม ตราเพชร ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B216-A10109') {
            $(this).text('ยิปซั่ม ST ขอบลาด 1.2x2.4 m.x9mm');
        }
        else if (data.text() == 'B216-A30109') {
            $(this).text('ยิปซั่ม ST ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B216-A40109') {
            $(this).text('ยิปซั่ม ST ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B217-A30109') {
            $(this).text('ยิปซั่ม 3G ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B218-A10109') {
            $(this).text('ยิปซั่ม Maxum ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B218-A30109') {
            $(this).text('ยิปซั่ม Maxum ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B218-A40109') {
            $(this).text('ยิปซั่ม Maxum ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B219-A10109') {
            $(this).text('ยิปซั่ม City bord ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B219-A30109') {
            $(this).text('ยิปซั่ม City bord ขอบลาด ทนชื้น1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B219-A40109') {
            $(this).text('ยิปซั่ม City bord ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B225-A30109') {
            $(this).text('ยิปซั่ม บิ๊ก-บอย ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B225-A40109') {
            $(this).text('ยิปซั่ม บิ๊ก-บอย ขอบลาด กันร้อน1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B226-A10209') {
            $(this).text('ยิปซั่ม ยิปแม๊ก ขอบลาด 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B227-A10195') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด 1.2x2.4m.x9.5');
        }
        else if (data.text() == 'B227-A10595') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด1.2x3.0mx9.5mm');
        }
        else if (data.text() == 'B227-A10895') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด 1.2x2.7m.x9.5');
        }
        else if (data.text() == 'B227-A30195') {
            $(this).text('ยิปซั่ม TRUSUS ขอบลาด ทนชื้น 1.2x2.4m.x9.5');
        }
        else if (data.text() == 'B227-B10209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B227-B10512') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ 1.22x3.05x12mm');
        }
        else if (data.text() == 'B227-B30209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ ทนชื้น 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B227-B30512') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ ทนชื้น1.22x3.05x12mm');
        }
        else if (data.text() == 'B227-B40209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ กันร้อน 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B227-B60209') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ ทนไฟ1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B228-A10495') {
            $(this).text('ยิปซั่ม OPSKY ขอบลาด1.2x3.0mx9.5mm');
        }
        else if (data.text() == 'B229-A10109') {
            $(this).text('ยิปซั่ม ตรา DD board ขอบลาด  1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B229-B10109') {
            $(this).text('ยิปซั่ม DD bord ขอบเรียบ 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B231-A10109') {
            $(this).text('ยิปซั่ม GM กัมพูชา ขอบลาด 1.2mx2.4mx9mm');
        }
        else if (data.text() == 'B232-A10109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B232-A10112') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B232-A10209') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B232-A30109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B232-A40109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด กันร้อน 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B232-AW0209') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด ทนชื้น WAX 1.22x2.44m.x9mm');
        }
        else if (data.text() == 'B232-AW0212') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบลาด ทนชื้น WAX 1.22x2.44m.x12mm');
        }
        else if (data.text() == 'B232-B10109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบเรียบ 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B232-B10112') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบเรียบ 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B232-B30109') {
            $(this).text('ยิปซั่ม ตรา TOA ขอบเรียบ ทนชื้น 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B233-A10109') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B233-A10112') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B233-A10512') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x3m.x12mm');
        }
        else if (data.text() == 'B233-A10709') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.3m.x9mm');
        }
        else if (data.text() == 'B233-A10712') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.3m.x12mm');
        }
        else if (data.text() == 'B233-A10812') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด 1.2x2.7m.x12mm');
        }
        else if (data.text() == 'B233-A30112') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด ทนชื้น 1.2x2.4m.x12mm');
        }
        else if (data.text() == 'B233-A30512') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด ทนชิ้น 1.2x3m.x12mm');
        }
        else if (data.text() == 'B233-A30812') {
            $(this).text('ยิปซั่ม ตรา HOFF GM ขอบลาด ทนชิ้น 1.2x2.7m.x12mm');
        }
        else if (data.text() == 'B234-A10109') {
            $(this).text('ยิปซั่ม DIC ขอบลาด 1.2x2.4m.x9mm');
        }
        else if (data.text() == 'B227-B10412') {
            $(this).text('ยิปซั่ม TRUSUS ขอบเรียบ กันร้อน 1.22x3.05m.x12mm');
        }
        else if (data.text() == 'B144-A10109') {
            $(this).text('ยิปซั่ม GM เบาๆ ขอบลาด 1.2x2.4m.x9mm');
        }
        else {
            $(this).text('ไม่พบข้อมูลชนิดสินค้า');
        }
    });
});
