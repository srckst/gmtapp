<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Validator;

use App\SETABLE;
use App\SOTABLE;
use App\USERSAVEDETAIL;
use App\Item;
use App\ItemSe;
use App\Fgbarcode;
use App\Itemdecode;
use App\Wipline1;
use App\BplusDB;
use App\AllOrder;
use App\FgTransfer;

use Carbon\Carbon;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index(){
        return view('home');
    }

    public function login(){
        return view ('login');
    }
    public function select(){

        $countso = 1;
        $countse = 1;
        $so = DB::connection('sqlsrv_bplus')->table('DOCINFO')
        ->join('AROE','DOCINFO.DI_KEY','=','AROE.AROE_DI')
        ->join('ARFILE','AROE.AROE_AR','=','ARFILE.AR_KEY')
        ->whereDate('DI_CRE_DATE', '>=', Carbon::now()->subDays(60))
        ->orderBy('DI_CRE_DATE','DESC')
        ->where('DI_REF', 'LIKE', 'SO%')
        ->get()->all();

        $se = DB::connection('sqlsrv_bplus')->table('DOCINFO')
        ->join('AROE','DOCINFO.DI_KEY','=','AROE.AROE_DI')
        ->join('ARFILE','AROE.AROE_AR','=','ARFILE.AR_KEY')
        ->whereDate('DI_CRE_DATE', '>=', Carbon::now()->subDays(60))
        ->orderBy('DI_CRE_DATE','DESC')
        ->where('DI_REF', 'LIKE', 'SE%')
        ->get()->all();
        return view('select', compact('so','se','countso','countse'));
    }
    public function cutstocks($DI_KEY){

        $i = 1;
        $n = 1;
        $n2 = 1;

        $getDB = DB::connection('sqlsrv_bplus')->table('SKUMASTER');

        $groupnum = Fgbarcode::select(DB::raw('left(fg_code, 11) as fg_code'))
        ->groupBy(DB::raw('left(fg_code, 11)'))
        ->where('DI_KEY','=',$DI_KEY)
        ->pluck('fg_code');

        $total = Fgbarcode::select(DB::raw("sum(cast(right(fg_code, 3) as int)) as fg_code"))
        ->groupBy(DB::raw('left(fg_code, 11)'))
        ->where('DI_KEY','=',$DI_KEY)
        ->pluck('fg_code');

        $showoder = DB::connection('sqlsrv_bplus')
        ->table('DOCINFO')
        ->where('DI_KEY','=',$DI_KEY)
        ->value('DI_REF');

        $getDI_KEY = DB::connection('sqlsrv_bplus')->table('DOCINFO')
        ->where('DI_KEY', '=', $DI_KEY)
        ->value('DI_KEY');

        $getAR_NAME = DB::connection('sqlsrv_bplus')->table('DOCINFO')
        ->join('AROE','DOCINFO.DI_KEY','=','AROE.AROE_DI')
        ->join('ARFILE','AROE.AROE_AR','=','ARFILE.AR_KEY')
        ->where('DI_KEY','=',$DI_KEY)
        ->value('ARFILE.AR_NAME');

        $fg = DB::table('fg_barcode')->where('DI_KEY','=',$DI_KEY)->get();

        // $price = DB::connection('sqlsrv_bplus')->table('DOCINFO')
        // ->join('TRANSTKH', 'DOCINFO.DI_KEY', '=', 'TRANSTKH.TRH_DI')
        // ->join('TRANSTKD', 'TRANSTKH.TRH_KEY', '=', 'TRANSTKD.TRD_TRH')
        // ->join('SKUMASTER', 'TRANSTKD.TRD_SKU', '=', 'SKUMASTER.SKU_KEY')
        // ->select(DB::raw('SUM(TRANSTKD.TRD_U_PRC) as prctotal'))
        // ->where('DOCINFO.DI_KEY',$DI_KEY)
        // ->groupBy('SKUMASTER.SKU_CODE')
        // ->value('TRD_U_PRC');

        //$decimoprc = number_format((float)$price,2,'.','');

        //select DOCINFO.DI_REF,TRANSTKD.TRD_U_PRC,SKUMASTER.SKU_CODE from SKUMASTER,TRANSTKH , DOCINFO , TRANSTKD where DOCINFO.DI_KEY = TRANSTKH.TRH_DI AND TRANSTKD.TRD_SKU = SKUMASTER.SKU_KEY AND
        //TRANSTKH.TRH_KEY = TRANSTKD.TRD_TRH AND DI_REF Like 'SO%'


        $view = view('tabcutout',compact('fg','i','n','n2','groupnum','total','showoder','getDI_KEY','getAR_NAME'));
        return $view;

    }
    public function transfer_input(Request $request){

        $fg_trans = new FgTransfer;

        $fg_trans->fg_code_transfer = $request->input('fg_code_transfer');

        $fg_trans->save();
    }
    public function transfer_delete($id){

        $delete = FgTransfer::find($id);
        $delete->delete();
        return $delete;
    }
    public function transfer(){

        $i = 1;
        $n = 1;
        $n2 = 1;

        $groupnumtrans = FgTransfer::select(DB::raw('left(fg_code_transfer, 11) as fg_code_transfer'))
        ->groupBy(DB::raw('left(fg_code_transfer, 11)'))
        ->pluck('fg_code_transfer');

        $total = FgTransfer::select(DB::raw("sum(cast(right(fg_code_transfer, 3) as int)) as fg_code_transfer"))
        ->groupBy(DB::raw('left(fg_code_transfer, 11)'))
        ->pluck('fg_code_transfer');

        $fg_trans = FgTransfer::all();

        $view = view('transfer',compact('fg_trans','i','n','n2','groupnumtrans','total'));
        return $view;
    }

    // Se Table
    public function home_se(){
        $se = SETABLE::all();
        return view('se.table_se', compact('se'));
    }

    public function reset_password(){
        return view('auth.password.reset');
    }
    public function email_password_reset(){
        return view('password.email');
    }

    public function selectfunction(){
        return view('select_function');
    }

    public function seshowmain(){
        $se_show = USERSAVEDETAIL::all();
        return view('testview', compact('se_show'));
    }

    public function viewitemact(){
        $item = Item::all();
        return view('view_item', compact('item'));
    }




    public function sedetail_search(Request $request){
        if($request->ajax())
        {
            $output = '';
            $query = $request->get('query');
            if($query != '')
            {
                $data = DB::table('se_table')
                ->where('id', 'like', '%'.$query.'%')->get();
            }
            else
            {
                $data = DB::table('se_table')
                ->orderBy('id')->get();
            }
            $total_row = $data->count();
            if($total_row > 0)
            {
                foreach($data as $row)
                {
                    $output .= '
                    <tr>
                        <td>'.$row->id.'</td>
                        <td>'.$row->name.'</td>
                        <td>'.$row->created_at.'</td>
                    </tr>
                    ';
                }
            }
            else
            {
                $output = '
                <tr>
                    <td align="center" colspan="5">No Data Found</td>
                </tr>
                ';
            }
            $data = array(
            'table_data'  => $output,
            'total_data'  => $total_row
            );

            echo json_encode($data);
        }
    }
    /*
    $se_fetch['se_fetch'] = DB::table('se_table')->get();

    if (count($se_fetch) > 0) {

    return view('se_detail',$se_fetch);
}
else {

    return view('se_detail');
}


$sesearch = $se_request->get('se_search');
$se_fetch = DB::table('se_table')->where('id' , 'like' , '%'.$sesearch.'%')->paginate(5);

return view('se_detail' , ['se_table' => $se_fetch]);
}

/*public function sesearch(Request $se_request){

    $sesearch = $se_request->get('se_search');
    $se_table = DB::table('se_table')->where('id' , 'like' , '%'.$sesearch.'%')->paginate(5);

    return view('se_detail' , ['se_table' => $se_table]);

}*/




public function datatable_se(){
    $se = SETABLE::all();
    return view('se.datatable_se', compact('se'));
}
public function ajax_se(){
    return view('se.ajax_se');
}
public function addse(Request $request){

    if ($request->isMethod('post')) {
        $sedetail=new USERSAVEDETAIL();
        $sedetail->id=$request->input('se_id');
        $sedetail->name=$request->input('se_name');
        $sedetail->created_at=$request->input('se_created_at');
        $sedetail->save();
    }
    return view('testview',[
    'sedetail' => $sedetail
    ]);

}

// So Table
public function home_so(){
    $so = SOTABLE::all();
    return view('so.table_so', compact('so'));

}

public function datatable_so(){
    $so = SOTABLE::all();
    return view('so.datatable_so', compact('so'));
}
public function ajax_so(){
    return view('so.ajax_so');
}

public function se_view_edit(){
    return view('se_detail', compact('se'));
}

public function viewitem(){
    return view('view_item');
}

public function getitemso($id){
    $item = Item::where('so_id','=',$id)->get();
    $showoder = SOTABLE::where('id','=',$id)->value('so_oder_number');
    return view('view_item_so', compact('item','showoder'));
}

public function create_so(){
    return view('create_so');
}
public function getitemse($id){
    $item = ItemSe::where('se_id','=',$id)->get();
    return view('view_item_se', compact('item','showoder'));
}

public function create_se($id){
    return view('create_se');
}

public function resultitem(){
    $re = Item::where('oder_number', '=', 'SO6208/0008')->get();

    dd($re);
}

public function resu(){
    $result = SOTABLE::all();

    return View::make('result')->with('result', $result);
}

public function wip_test(){
    $view = view('test_wip');
    return $view;
}

public function fetch_fg(){
    $fg = Fgbarcode::all();
    $i = 1;
    $n = 1;
    $groupnum = Fgbarcode::select(DB::raw('left(fg_code, 11) as fg_code'))
    ->groupBy(DB::raw('left(fg_code, 11)'))
    ->pluck('fg_code');

    $total = Fgbarcode::select(DB::raw("sum(right(fg_code, 3)) as fg_code"))
    ->groupBy(DB::raw('left(fg_code, 11)'))
    ->pluck('fg_code');

    $view = view('tabcutout',compact('fg','i','n','groupnum','total'));
    return $view;
}

public function viewitemdecode(){
    $i=1;
    $itemcode = Itemdecode::all();
    $view = view('itemdecode', compact('itemcode','i'));
    return $view;
}

public function itemdecode(Request $request){
    $decode = new Itemdecode;

    $decode->item_code = $request->input('item_code');

    $decode->save();
}

public function selectwipline(){
    $view = view('select_line');
    return $view;
}

public function deletewipline1($id){
    $delete = Wipline1::find($id);
    $delete->delete();
    return $delete;
}

public function deletefg($id){
    $delete = Fgbarcode::find($id);
    $delete->delete();
    return $delete;
}

public function printwipl1a($id){
    $n =  1;
    $n2 = 1;
    $n3 = 1;
    $padnum = 1;
    $padnum2 = 1;
    $padnum3 = 1;
    $wip1 = Wipline1::where('id','=',$id)->get();
    $view = view('printwipline1a',compact('wip1','n','n2','n3','padnum','padnum2','padnum3'));
    return $view;
}
public function connectdbplus(){
    $count = 1;
    $db_ext = \DB::connection('sqlsrv');
    $data = $db_ext->table('DOCINFO')->orderBy('DI_KEY')->chunk(100, function($db) {
        foreach ($db as $dbs) {
            dd($db);
        }
    });

    $view = view('bplustestview');
    return $view;
}


public function viewoderall(){
    $the_view = "";
    /*DB::table('DOCINFO')->select('DI_KEY','DI_REF')
    ->orderBy('DI_KEY')->chunk(100, function ($user_chunk) use (&$the_view) {
        foreach ($user_chunk as $user) {
            $the_view .= "<tr ><td >" .$user->DI_KEY. "</td >" .
                "<td >" . $user->DI_REF . "</td >" .
                    "<td > " .
                        " </td ></tr >";
                    }

                });
                */
                $con = DB::connection('sqlsrv_bplus')->table('DOCINFO')->where('DI_REF', 'LIKE', 'SO%')->get();
                dd($con);


                // AllOrder::orderBy('DI_KEY')->chunk(100, function ($chunked_results) use (&$the_view) {
                    //     foreach ($chunked_results as $results) {
                        //         $the_view .= "<tr ><td >" .$results->DI_KEY. "</td >" .
                            //             "<td >" . $results->DI_REF . "</td >" .
                                //             "<td > " .
                                    //             " </td ></tr >";
                                    //      }
                                    // });
                                    return view('vieworder', compact('the_view'));

                                }



                            }
