<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fgbarcode extends Model
{
    public $table = "fg_barcode";

    protected $fillable = [
        'id','fg_code',
    ];
}
