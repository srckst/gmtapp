@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="table-responsive">
                        <div class="container">
                            <div class="print-output">
                                @foreach ($wip1 as $barcodes)
                                    <input type="text" name="" id="output1aid" value="{{ $barcodes->id }}">
                                    <table class="table-output">
                                        <tr>
                                            <td colspan="4" class="tb-sky   "></br></br></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"><center>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr($barcodes->wl1_barcode,4,7).substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT).$barcodes->wl1_amount, 'C128',1,35) !!}</center><small>{{"B1".$barcodes->wl1_brand.substr($barcodes->wl1_barcode,4,7) }}:1.2 x 2.4 m.:{{substr("$barcodes->wl1_barcode",11,6)}}{{str_pad($padnum3++,4,"0",STR_PAD_LEFT)}}:43596</small>
                                            </br></br>
                                            <div class="fix-row-center">
                                                <div class="fix-grid-left"><small>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr("$barcodes->wl1_barcode",4,7), 'C128',1,35) !!}</small><small class="outputitemfg1">{{ $barcodes->wl1_barcode }}{{ $barcodes->wl1_brand }}{{ $barcodes->wl1_amount }}</small></div>
                                                <div class="fix-grid-right"><small>{!! DNS1D::getBarcodeHTML(substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT), 'C128',1,35) !!}</small><small>{{substr("$barcodes->wl1_barcode",11,6)}}{{str_pad($padnum2++,4,"0",STR_PAD_LEFT)}}</small></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="tb-sky wip-fix-fontsize-output"><b>รหัสสินค้า</b></th>
                                        <td colspan="3" class="tb-sky"></td>
                                    </tr>
                                    <tr>
                                        <th class="tb-sky wip-fix-fontsize-output"><b>ชื่อสินค้า</b></th>
                                        <td colspan="3" class="tb-sky"></td>
                                    </tr>
                                    <tr>
                                        <th class="tb-sky wip-fix-fontsize-output"><b>วันที่คัด</b></th>
                                        <td colspan="3" class="tb-sky"></td>
                                    </tr>
                                    <tr>
                                        <th class="tb-sky wip-fix-fontsize-output">ชนิดแผ่น</th>
                                        <td class="tb-sky"></td>
                                        <td class="tb-sky"></td>
                                        <td class="tb-sky"></td>
                                    </tr>
                                    <tr>
                                        <th class="tb-sky wip-fix-fontsize-output">ขอบ</th>
                                        <td class="tb-sky"></td>
                                        <td class="tb-sky"></td>
                                        <td class="tb-sky"></td>
                                    </tr>
                                    <tr>
                                        <th class="wip-fix-fontsize-output">หมวดสินค้า</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="wip-fix-fontsize-output">Lot.</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="wip-fix-fontsize-output">จำนวน</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="wip-fix-fontsize-output">เวลา</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="wip-fix-fontsize-output">น้ำหนัก</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </br>
                            <input type="text" name="" id="output1aid" value="{{ $barcodes->id }}">
                            <table class="table-output">
                                <tr>
                                    <td class="tb-purple">
                                        <td class="noline1" cellspacing="0" cellpadding="0">
                                            <td class="noline2" cellspacing="0" cellpadding="0">
                                                <td class="tb-purple"></br></br></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><center>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr($barcodes->wl1_barcode,4,7).substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT).$barcodes->wl1_amount, 'C128',1,35) !!}</center><small>B113-A10109:1.2 x 2.4 m.:1905112071:43596</small>
                                                </br></br>
                                                <div class="fix-row-center">
                                                    <div class="fix-grid-left"><small>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr("$barcodes->wl1_barcode",4,7), 'C128',1,35) !!}</small><small class="outputitemfg1">{{ $barcodes->wl1_barcode }}{{ $barcodes->wl1_brand }}{{ $barcodes->wl1_amount }}</small></div>
                                                    <div class="fix-grid-right"><small>{!! DNS1D::getBarcodeHTML(substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT), 'C128',1,35) !!}</small><small>{{substr("$barcodes->wl1_barcode",11,6)}}{{str_pad($padnum2++,4,"0",STR_PAD_LEFT)}}</small></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output"><b>รหัสสินค้า</b></th>
                                            <td colspan="3" class=""></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output"><b>ชื่อสินค้า</b></th>
                                            <td colspan="3" class=""></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output"><b>วันที่คัด</b></th>
                                            <td colspan="3" class=""></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">ชนิดแผ่น</th>
                                            <td class=""></td>
                                            <td class=""></td>
                                            <td class=""></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">ขอบ</th>
                                            <td class=""></td>
                                            <td class=""></td>
                                            <td class=""></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">หมวดสินค้า</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">Lot.</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">จำนวน</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">เวลา</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">น้ำหนัก</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </br></br></br>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
