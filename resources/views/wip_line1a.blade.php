
@extends('layouts.app')

@section('content')


    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="text-left">
                    <a href="{{ route('main') }}" class="btn btn-warning"  name="button"><em class="text-white fa fa-home"><b>  กลับไปยังเมนูหลัก</b></em></a>
                </div>
                <h2><b>ระบบสแกนบาร์โค้ด TAG WIP</b></h2>
                <div class="text-center">
                    <ul class="nav nav-tabs">
                        <li class="active tab-size-sm"><a href="#barcode"><h4>กรอกรหัส TAG WIP</h4></a></li>
                        <li class="tab-size-xs"><a href="#detail"><h4>ข้อมูล</h4></a></li>
                        <li class="tab-size-xs"><a href="#tofg"><h4>รหัส FG</h4></a></li>
                        <li class="tab-size-sm"><a href="#output"><h4>บาร์โค้ด TAG FG</h4></a></li>
                    </ul>
                </div>
            </br>
            <div class="tab-content">
                <div id="barcode" class="tab-pane fade in active">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <form class="form-inline md-form form-sm mt-0">
                                    <input id="myInputCode" onkeyup="myFunction()" type="text" class="form-control" placeholder="ค้นหารหัสบาร์โค้ด">
                                    <button class="btn btn-primary btn-md" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-8">
                                <span id="form_result"></span>
                                <form id="insertwipline1" class="form-inline md-form form-sm mt-0 text-right" enctype="multipart/form-data" method="post">
                                    {{ csrf_field() }}
                                    <div class="text-right">
                                        @include('frontend.selectbrand')
                                        <input type="number" name="wl1_amount" id="wl1_amount" class="form-control text-center" value="100" data-toggle="tooltip" title="จำนวนที่คัดแล้ว" placeholder="จำนวนที่คัดแล้ว" required/>
                                        <input id="wl1_barcode" name="wl1_barcode" type="text" class="form-control text-center" data-toggle="tooltip" title="สแกนบาร์โค้ด" style="width:30%;" placeholder="สแกนบาร์โค้ดตรงนี้" required/>
                                        <input type="hidden" name="action" id="action" />
                                        <input type="hidden" name="hidden_id" id="hidden_id" />
                                        <button id="subline1" type="submit" class="btn btn-dark btn-md" name="submit_fgcode">
                                            เพิ่มข้อมูล
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="myTableCode" class="table table-hover bg-white text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>บาร์โค้ด</th>
                                        <th>Brand Code</th>
                                        <th>จำนวนที่คัดแล้ว</th>
                                        <th>ตรวจสอบไลน์ผลิต</th>
                                        <th>ไลน์กะที่คัด</th>
                                        <th><i class="fa fa-cog"></i></th>
                                    </tr>
                                </thead>
                                <tbody id="searchCode">
                                    @foreach ($wip1 as $barcodes)
                                        <tr>
                                            <td style="display:none">{{ $barcodes->id }}</td>
                                            <td>{{ $n3++ }}</td>
                                            <td class="wipline1code">{{ $barcodes->wl1_barcode }}</center></td>
                                            <td class="brandline1">{{ $barcodes->wl1_brand }}</td>
                                            <td class="amountline1">{{ $barcodes->wl1_amount }}</td>
                                            <td class="checkline1">{{ $barcodes->wl1_barcode }}</td>
                                            <td>1A</td>
                                            <td><a data-target="#editline1a" data-toggle="modal" class="btn btn-default btn-sm fa fa-edit" data-toggle="tooltip" title="แก้ไขข้อมูล" style="font-size:15px;"></a>
                                                <a href="#" class="btn btn-danger btn-sm fa fa-trash deleteline1" data-toggle="tooltip" title="ลบข้อมูล" style="font-size:15px;"></a>
                                                <a id="printtagwipline1a" class="btn btn-success btn-sm fa fa-print printtagwipline1a" data-toggle="tooltip" title="พิมพ์" style="font-size:15px;"></a>
                                            </td><!--href="{{ route('printwipl1a',$barcodes->id) }}"-->
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>บาร์โค้ด</th>
                                        <th>Brand Code</th>
                                        <th>จำนวนที่คัดแล้ว</th>
                                        <th>ตรวจสอบไลน์ผลิต</th>
                                        <th>ไลน์กะที่คัด</th>
                                        <th><i class="fa fa-cog"></i></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="detail" class="tab-pane fade">
                    <div class="container-fluid">
                        <form class="form-inline md-form form-sm mt-0">
                            <input id="myInputCut" onkeyup="myFunction()" type="text" class="form-control" placeholder="ค้นหา">
                            <button class="btn btn-primary btn-md" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                            <p class="text-danger">หมายเหตุ : สามารถค้นหาด้วย รหัสซื้อขาย จำนวน ตำแหน่งเก็บ และเลขที่ล็อต</p>
                        </form>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-hover bg-white text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="width:20%;">รหัสบาร์โค้ด</th>
                                        <th style="width:10%;">รหัสสินค้า</th>
                                        <th>ประเภทสินค้า</th>
                                        <th>วันที่</th>
                                        <th>ตั้งเลขที่</th>
                                        <th>จำนวน</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($wip1 as $barcodes)
                                        <tr>
                                            <td>{{ $n++ }}</td>
                                            <td>{{ $barcodes->wl1_barcode }}</td>
                                            <td class="codewip2">{{ $barcodes->wl1_barcode }}</td>
                                            <td class="codewip3">{{ $barcodes->wl1_barcode }}</td>
                                            <td class="codewip4">{{ $barcodes->wl1_barcode }}</td>
                                            <td class="codewip5">{{ $barcodes->wl1_barcode }}</td>
                                            <td class="codewip6">{{ $barcodes->wl1_barcode }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>รหัสบาร์โค้ด</th>
                                        <th>รหัสสินค้า</th>
                                        <th>ประเภทสินค้า</th>
                                        <th>วันที่</th>
                                        <th>ตั้งเลขที่</th>
                                        <th>จำนวน</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="tofg" class="tab-pane fade">
                    <div class="container-fluid">
                        <form class="form-inline md-form form-sm mt-0">
                            <input id="myInputOut" onkeyup="myFunction()" type="text" class="form-control" placeholder="ค้นหา">
                            <button class="btn btn-primary btn-md" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                            <p><small class="text-danger">หมายเหตุ : สามารถค้นหาด้วย รหัสซื้อขาย จำนวน ตำแหน่งเก็บ และเลขที่ล็อต</small></p>
                        </form>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-hover bg-white text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>LOT FG</th>
                                        <th>OUT FG CODE</th>
                                        <th>ตรวจโค้ดประเภทแผ่น</th>
                                        <th>วันที่คัด</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($wip1 as $barcodes)
                                        <tr>
                                            <td>{{ $n2++ }}</td>
                                            <td class="outfg1">{{ $barcodes->wl1_barcode }}</td>
                                            <td class="outfg2">{{ $barcodes->wl1_barcode }}{{ $barcodes->wl1_brand }}{{ $barcodes->wl1_amount }}</td>
                                            <td class="outfg3">{{ $barcodes->wl1_barcode }}{{ $barcodes->wl1_brand }}</td>
                                            <td class="outfg4">{{ $barcodes->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>LOT FG</th>
                                        <th>OUT FG CODE</th>
                                        <th>ตรวจโค้ดประเภทแผ่น</th>
                                        <th>วันที่คัด</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="output" class="tab-pane fade">
                    <div class="container-fluid">
                        <div class="table-responsive">
                            <div class="container">
                                <div class="print-output">
                                    @foreach ($wip1 as $barcodes)
                                        <input type="hidden" name="" id="output1aid" value="{{ $barcodes->id }}">
                                        <table class="table-output">
                                            <tr>
                                                <td colspan="4" class="tb-sky   "></br></br></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><center>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr($barcodes->wl1_barcode,4,7).substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT).$barcodes->wl1_amount, 'C128',1,35) !!}</center><small>{{"B1".$barcodes->wl1_brand.substr($barcodes->wl1_barcode,4,7) }}:1.2 x 2.4 m.:{{substr("$barcodes->wl1_barcode",11,6)}}{{str_pad($padnum3++,4,"0",STR_PAD_LEFT)}}:43596</small>
                                                </br></br>
                                                <div class="fix-row-center">
                                                    <div class="fix-grid-left"><small>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr("$barcodes->wl1_barcode",4,7), 'C128',1,35) !!}</small><small class="outputitemfg1">{{ $barcodes->wl1_barcode }}{{ $barcodes->wl1_brand }}{{ $barcodes->wl1_amount }}</small></div>
                                                    <div class="fix-grid-right"><small>{!! DNS1D::getBarcodeHTML(substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT), 'C128',1,35) !!}</small><small>{{substr("$barcodes->wl1_barcode",11,6)}}{{str_pad($padnum2++,4,"0",STR_PAD_LEFT)}}</small></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="tb-sky wip-fix-fontsize-output"><b>รหัสสินค้า</b></th>
                                            <td colspan="3" class="tb-sky"></td>
                                        </tr>
                                        <tr>
                                            <th class="tb-sky wip-fix-fontsize-output"><b>ชื่อสินค้า</b></th>
                                            <td colspan="3" class="tb-sky"></td>
                                        </tr>
                                        <tr>
                                            <th class="tb-sky wip-fix-fontsize-output"><b>วันที่คัด</b></th>
                                            <td colspan="3" class="tb-sky"></td>
                                        </tr>
                                        <tr>
                                            <th class="tb-sky wip-fix-fontsize-output">ชนิดแผ่น</th>
                                            <td class="tb-sky"></td>
                                            <td class="tb-sky"></td>
                                            <td class="tb-sky"></td>
                                        </tr>
                                        <tr>
                                            <th class="tb-sky wip-fix-fontsize-output">ขอบ</th>
                                            <td class="tb-sky"></td>
                                            <td class="tb-sky"></td>
                                            <td class="tb-sky"></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">หมวดสินค้า</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">Lot.</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">จำนวน</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">เวลา</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th class="wip-fix-fontsize-output">น้ำหนัก</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </br>
                                <input type="hidden" name="" id="output1aid" value="{{ $barcodes->id }}">
                                <table class="table-output">
                                    <tr>
                                        <td class="tb-purple">
                                            <td class="noline1" cellspacing="0" cellpadding="0">
                                                <td class="noline2" cellspacing="0" cellpadding="0">
                                                    <td class="tb-purple"></br></br></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4"><center>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr($barcodes->wl1_barcode,4,7).substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT).$barcodes->wl1_amount, 'C128',1,35) !!}</center><small>B113-A10109:1.2 x 2.4 m.:1905112071:43596</small>
                                                    </br></br>
                                                    <div class="fix-row-center">
                                                        <div class="fix-grid-left"><small>{!! DNS1D::getBarcodeHTML("B1".$barcodes->wl1_brand.substr("$barcodes->wl1_barcode",4,7), 'C128',1,35) !!}</small><small class="outputitemfg1">{{ $barcodes->wl1_barcode }}{{ $barcodes->wl1_brand }}{{ $barcodes->wl1_amount }}</small></div>
                                                        <div class="fix-grid-right"><small>{!! DNS1D::getBarcodeHTML(substr("$barcodes->wl1_barcode",11,6).str_pad($padnum++,4,"0",STR_PAD_LEFT), 'C128',1,35) !!}</small><small>{{substr("$barcodes->wl1_barcode",11,6)}}{{str_pad($padnum2++,4,"0",STR_PAD_LEFT)}}</small></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output"><b>รหัสสินค้า</b></th>
                                                <td colspan="3" class=""></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output"><b>ชื่อสินค้า</b></th>
                                                <td colspan="3" class=""></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output"><b>วันที่คัด</b></th>
                                                <td colspan="3" class=""></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">ชนิดแผ่น</th>
                                                <td class=""></td>
                                                <td class=""></td>
                                                <td class=""></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">ขอบ</th>
                                                <td class=""></td>
                                                <td class=""></td>
                                                <td class=""></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">หมวดสินค้า</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">Lot.</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">จำนวน</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">เวลา</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="wip-fix-fontsize-output">น้ำหนัก</th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </br></br></br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="notideleteline1" tabindex="-1" role="dialog" aria-labelledby="DeleteBarcodeLine1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="DeleteBarcodeLine1">ลบข้อมูลบาร์โค้ด</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="deletfieldline1">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}

                    <input type="hidden" name="id" id="delete_line1id">
                    <p>คุณต้องการลบข้อมูลบาร์โค้ดหรือไม่</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-danger">ลบบาร์โค้ด</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editline1a">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">เพิ่มข้อมูล</h4>
                <h4>Barcode : </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="panel-body">
                <a class="btn btn-default" id="addl1a" href="#" role="button"><span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มของที่เสียง</a>
                    <form class="form-inline md-form form-sm mt-0">
                    <div class="table-responsive">
                        <table class="table" id="wipline1awaste">
                            <tr>
                                <th>ของเสีย</th>
                                <th>จำนวนที่เสีย</th>
                            </tr>
                        </table>
                    </div>
                </form>

                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

@endsection
