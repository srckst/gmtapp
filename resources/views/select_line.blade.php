@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="text-left">
                    <a href="{{ route('main') }}" class="btn btn-warning"  name="button"><em class="text-white fa fa-home"><b>  กลับไปยังเมนูหลัก</b></em></a>
                </div>
                <h2 class="header-select"><b>เลือกไลน์การผลิต</b></h2>
                <p class="text-danger">เรียนแจ้งผู้ใช้ระบบทุกท่าน : เพื่อการทำงานที่สมบูรณ์ กรุณาใช้ google chrome ในการใช้ระบบ</p>
            </br>
            <div class="container-fluid" style="width:90%;">
                <div class="row">
                    <div class="col-lg-6 col-xs-6 text-white">
                        <!-- small box -->
                        <a href="/wipline1">
                            <div class="small-box bg-aqua card-shadow">
                                <div class="inner">
                                    <br>
                                    <h3 class="text-center text-so-white" style="font-size:2.2vw;">ไลน์ที่ 1</h3>
                                    <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-clipboard-list"></i>
                                </div>
                                <a href="/wipline1" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <a href="/wipline2">
                            <div class="small-box bg-green card-shadow">
                                <div class="inner">
                                    <br>
                                    <h3 class="text-center text-so-white" style="font-size:2.2vw;">ไลน์ที่ 2</h3>
                                    <p class="text-center text-so-white">Lorem ipsum dolor sit amet, consectetur </p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-clipboard-list"></i>
                                </div>
                                <a href="/wipline2" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


@endsection
