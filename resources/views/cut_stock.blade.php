@extends('layouts.app')

@section('content')

    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="card-header bg-white"><h4><b>บาร์โค้ดรหัส : </b></h4>
                        <a href="{{ route('select') }}" class="text-white btn btn-warning fa fa-search"><b>&nbsp;&nbsp;&nbsp;กลับไปหน้าค้นหา</b></a>
                    </div>
                </div>
            </br>
            <div class="container-fluid">
                <p><b>รายการตัดสต็อก</b></p>
                <form class="form-inline md-form form-sm mt-0">
                    <input id="myInput" onkeyup="myFunction()" type="text" class="form-control" placeholder="รหัสสินค้า">
                    <button class="btn btn-primary btn-md" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </form>

                <table id="myTable" class="table table-hover bg-white text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>รหัสซื้อขาย</th>
                            <th>เลขที่ล็อต</th>
                            <th>จำนวน</th>
                            <th>ตำแหน่งเก็บ</th>
                        </tr>
                    </thead>
                    <tbody>

                            <tr>
                                <td id='countRow'></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>รหัสซื้อขาย</th>
                            <th>เลขที่ล็อต</th>
                            <th>จำนวน</th>
                            <th>ตำแหน่งเก็บ</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
