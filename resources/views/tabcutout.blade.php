@extends('layouts.app')

@section('content')
    <div class="container-fluid bg-white">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="text-left">
                    <a href="{{ route('main') }}" class="btn btn-warning"  name="button"><em class="text-white fa fa-th"><b>  กลับไปยังเมนูหลัก</b></em></a>
                    <a href="{{ route('select') }}" class="btn btn-default"  name="button"><em class="text-white fa fa-arrow-left"><b>  กลับไปก่อนหน้า</b></em></a>
                    {{-- <a href="{{ route('main') }}" class="btn btn-warning"  name="button"><em class="text-white fa fa-file-text"><b>  เลือกใบจอง</b></em></a> --}}
                </div>
                <h2><b class="changeheader">แปรรหัสออกของในประเทศ</b></h2>
                <div class="text-center">
                    <ul class="nav nav-tabs">
                        <li class="active tab-size-sm"><a href="#barcodefg"><h4>บาร์โค้ด</h4></a></li>
                        <li class="tab-size-md"><a href="#cut"><h4>ตัดสต้อก</h4></a></li>
                        <li class="tab-size-md"><a href="#out"><h4 class="conseorso">ออกของ</h4></a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div id="barcodefg" class="tab-pane fade in active">
                        <div class="container-fluid">
                        </br>
                        <h4 class="showoderno"><b>รหัสใบจอง : {{ $showoder }}</b><h4>
                            <h4><b>ชื่อลูกค้า : {{ $getAR_NAME }}</b><h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form class="form-inline md-form form-sm mt-0">
                                            <input id="myInputCode" onkeyup="myFunction()" type="text" class="form-control" placeholder="ใส่รหัสบาร์โค้ด">
                                            <button class="btn btn-primary btn-md" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <span id="form_result"></span>
                                        <form onsubmit="return validacija()" id="insertbarcode" class="form-inline md-form form-sm mt-0 text-right" enctype="multipart/form-data" method="post">
                                            @csrf
                                            <div class="text-right">
                                                <input class="form-control text-center" type="hidden" id="DI_REF" name="DI_REF" value="{{ $showoder }}">
                                                <input class="form-control text-center" type="hidden" id="DI_KEY" name="DI_KEY" value="{{ $getDI_KEY }}">
                                                <input id="fg_code" name="fg_code" type="text" class="form-control text-center inputfgcode" style="width:40%;" placeholder="สแกนบาร์โค้ดตรงนี้"/>
                                                <input type="hidden" name="action" id="action" />
                                                <input type="hidden" name="hidden_id" id="hidden_id" />
                                                <button id="submitfg" type="submit" class="btn btn-dark btn-md" name="submit_fgcode">
                                                    <i class="fa fa-barcode"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="myTableCode" class="table table-hover bg-white text-center">
                                        <thead>
                                            <tr>
                                                <th class="tab-size-mn">#</th>
                                                <th class="tab-size-lg">บาร์โค้ด</th>
                                                <th class="fa fa-cog"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="searchCode">
                                            @foreach ($fg as $fgcode )
                                                <tr>
                                                    <td style="display:none">{{ $fgcode->id }}</td>
                                                    <td style="display:none">{{ $fgcode->DI_KEY }}</td>
                                                    <td>{{ $n2++ }}</td>
                                                    <td class="fgcodefull">{{ $fgcode->fg_code }}</td>
                                                    <td><a href="#" class="btn btn-default btn-sm fa fa-edit" data-toggle="tooltip" title="แก้ไขข้อมูล" style="font-size:15px;"></a>  <a href="#" class="btn btn-danger btn-sm fa fa-trash deletefg" data-toggle="tooltip" title="ลบข้อมูล" style="font-size:15px;"></a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>บาร์โค้ด</th>
                                                <th class="fa fa-cog"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="cut" class="tab-pane fade">
                            <div class="container-fluid">
                            </br>
                            <h4><b>รายการตัดสต็อก</b></h4>
                            <form class="form-inline md-form form-sm mt-0">
                                <input id="myInputCut" onkeyup="myFunction()" type="text" class="form-control" placeholder="ค้นหา">
                                <button class="btn btn-primary btn-md" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                                <p class="text-danger">หมายเหตุ : สามารถค้นหาด้วย รหัสซื้อขาย จำนวน ตำแหน่งเก็บ และเลขที่ล็อต</p>
                            </form>
                            <div class="table-responsive">
                                <table id="myTableCut" class="table table-hover bg-white text-center">
                                    <thead>
                                        <tr>
                                            <th>รหัสซื้อขาย</th>
                                            <th>เลขที่ล็อต</th>
                                            <th>จำนวน</th>
                                            <th>รหัสแปรรูป/ผลผลิต</th>
                                            <th>ตำแหน่งเก็บ</th>
                                        </tr>
                                    </thead>
                                    <tbody id="searchCut">
                                        @foreach ($fg as $fgcode )
                                            <p style="display:none">{{ $fgcode->DI_KEY }}</p>
                                            <tr>
                                                <td class="fg1">{{ $fgcode->fg_code }}</td>
                                                <td class="fg2">{{ $fgcode->fg_code }}</td>
                                                <td class="fg3">{{ $fgcode->fg_code }}</td>
                                                <td class="fgp">{{ $fgcode->fg_code }}</td>
                                                <td class="fg4">{{ $fgcode->fg_code }}</td>
                                            </tr>
                                        @endforeach
                                        @foreach ($groupnum as $key => $row )
                                            <tr>
                                                <td class="fgt">{{ $row }}</td>
                                                <td class="nothingfg">{{ $row }}</td>
                                                <td class="fgamo">{{ $total[$key] }}{{ $row }}</td>
                                                <td class="fgp2">{{ $row }}</td>
                                                <td class="fg4n">{{ $row }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>รหัสซื้อขาย</th>
                                            <th>เลขที่ล็อต</th>
                                            <th>จำนวน</th>
                                            <th>รหัสแปรรูป/ผลผลิต</th>
                                            <th>ตำแหน่งเก็บ</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="out" class="tab-pane fade">
                        <div class="container-fluid">
                        </br>
                        <p><h4><b>รายการออกของ</b><h4></p>
                            <form class="form-inline md-form form-sm mt-0">
                                <input id="myInputOut" onkeyup="myFunction()" type="text" class="form-control" placeholder="ค้นหา">
                                <button class="btn btn-primary btn-md" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                                <p><small class="text-danger">หมายเหตุ : สามารถค้นหาด้วย รหัสซื้อขาย จำนวน ตำแหน่งเก็บ และเลขที่ล็อต</small></p>
                            </form>
                            <div class="table-responsive">
                                <table id="myTableOut" class="table table-hover bg-white text-center">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>รหัสซื้อขาย</th>
                                            <th>จำนวน</th>
                                            <th>ราคาต่อหน่วย</th>
                                            <th>ตำแหน่งเก็บ</th>
                                            <th>ลดต่อรายการ</th>
                                            <th>เลขล็อต</th>
                                        </tr>
                                    </thead>

                                    <tbody id="searchOut">
                                        @foreach ($groupnum as $key => $row )
                                            <tr>
                                                <td>1</td>
                                                <td class="fgtod">{{ $row }}</td>
                                                <td>{{ $total[$key] }}</td>
                                                <td class="fg7"></td>
                                                <td class="fg8"></td>
                                                <td class="fg9"></td>
                                                <td class="fg10"></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>รหัสซื้อขาย</th>
                                            <th>จำนวน</th>
                                            <th>ราคาต่อหน่วย</th>
                                            <th>ตำแหน่งเก็บ</th>
                                            <th>ลดต่อรายการ</th>
                                            <th>เลขที่ล็อต</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <button id="save-excel" class="btn btn-success" type="button" name="button">บันทึก Excel ตัดสต้อก</button>
                <button id="save-excel2" class="btn btn-success" type="button" name="button">บันทึก Excel ออกของ</button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="notideletefg" tabindex="-1" role="dialog" aria-labelledby="DeleteBarcodeFg" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="DeleteBarcodeFg">ลบข้อมูลบาร์โค้ด</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deletfieldfg">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}

                        <input type="hidden" name="id" id="delete_idfg">
                        <p>คุณต้องการลบข้อมูลบาร์โค้ดหรือไม่</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-danger">ลบบาร์โค้ด</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var filename = "{{ $showoder }}";

    </script>

@endsection
