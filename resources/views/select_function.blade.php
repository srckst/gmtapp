@extends('layouts.app')

@section('content')
    <div id="app">
        <lodingspinner></lodingspinner>
    </div>

<div class="container-fluid bg-white">
    <div class="panel panel-default">
        <div class="panel-body">
            <h2><b>เมนูหลัก</b></h2>
            <p class="text-danger">เรียนแจ้งผู้ใช้ระบบทุกท่าน : เพื่อการทำงานที่สมบูรณ์ กรุณาใช้ google chrome ในการใช้ระบบ</p>
        </br>
        <div class="container-fluid" style="width:90%;">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <a href="{{ route('select') }}">
                        <div class="small-box bg-blue card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                                <p class="text-so-white text-center">ระบบจัดการในส่วนของการออกของในประเทศ โอนย้ายในประเทศ และออกของนอกประเทศ</p>
                            </div>
                            <div class="icon">
                                <i class='fas fa-warehouse' style='font-size:55px;'></i>
                            </div>
                            <a href="{{ route('select') }}" data-toggle="modal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <a data-target="#selectworktime" data-toggle="modal">
                        <div class="small-box bg-green card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ระบบคัดบอร์ด</b></p>
                                <p class="text-so-white text-center">ระบบจัดการในส่วนการคัดแยกสินค้าเพื่อแปลงรหัสเป็น FG </p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-clipboard-check" style='font-size:60px;'></i>
                            </div>
                            <a data-target="#selectworktime" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <a href="{{ route('viewoderall') }}">
                        <div class="small-box bg-yellow card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ข้อมูลใบจองทั้งหมด</b></p>
                                <p class="text-so-white text-center">แสดงข้อมูลใบจองที่มีอยู่ทั้งหมดในระบบ</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-dolly" style='font-size:60px;'></i>
                            </div>
                            <a href="{{ route('viewoderall') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-xs-6">

                    <a href="{{ route('decodeitem') }}">
                        <div class="small-box bg-red card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ระบบตรวจสอบชนิดสินค้า</b></p>
                                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-home" style='font-size:60px;'></i>
                            </div>
                            <a href="{{ route('decodeitem') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>

                {{-- <div class="col-lg-3 col-xs-6">
                    <a href="{{ route('viewoderall') }}">
                        <div class="small-box bg-aqua card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ข้อมูลใบจองทั้งหมด</b></p>
                                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-home" style='font-size:60px;'></i>
                            </div>
                            <a href="{{ route('viewoderall') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div> --}}
<!--
                <div class="col-lg-3 col-xs-6">

                    <a href="{{ route('select') }}">
                        <div class="small-box bg-green card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-xs-6">

                    <a href="{{ route('select') }}">
                        <div class="small-box bg-yellow card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>

                <div class="col-lg-3 col-xs-6">

                    <a href="{{ route('select') }}">
                        <div class="small-box bg-red card-shadow">
                            <div class="inner">
                                <br>
                                <p class="text-so-white text-center"><b>ระบบสนับสนุนการทำงานส่วนงานคลังสินค้า</b></p>
                                <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <a href="{{ route('select') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </a>
                </div>
            -->
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="selectworktime">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h3 class="modal-title">เลือกช่วงเวลาการทำงาน (กะ)</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="text-danger">เลือกช่วงเวลาทำงานตามความเป็นจริง</p>
            </div>

            <!-- Modal body -->
            <div class="panel-body">
                <div class="container-fluid" style="width:90%;">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6 text-white">
                            <!-- small box -->
                            <a href="/gypman-tech/wipline1a">
                                <div class="small-box bg-orange card-shadow">
                                    <div class="inner">
                                        <br>
                                        <h3 class="text-center text-so-white" style="font-size:2.2vw;">A</h3>
                                        <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                    </div>

                                    <a href="/wipline1a" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </a>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-6 col-xs-6">
                            <!-- small box -->
                            <a href="/wipline2">
                                <div class="small-box bg-purple card-shadow">
                                    <div class="inner">
                                        <br>
                                        <h3 class="text-center text-so-white" style="font-size:2.2vw;">B</h3>
                                        <p class="text-center text-so-white">Lorem ipsum dolor sit amet, consectetur </p>
                                    </div>

                                    <a href="/wipline2" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cutstockfg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h3 class="modal-title">เลือกประเภทของข้อมูลใบจอง</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="text-danger">เลือกประเทศของข้อมูลใบจองตามความเป็นจริง</p>
            </div>

            <!-- Modal body -->
            <div class="panel-body">
                <div class="container-fluid" style="width:90%;">
                    <div class="row">
                        <div class="col-lg-6 col-xs-6 text-white">
                            <!-- small box -->
                            <a href="/tagfg">
                                <div class="small-box bg-orange card-shadow">
                                    <div class="inner">
                                        <br>
                                        <h3 class="text-center text-so-white" style="font-size:1.5vw;">ในประเทศ (SO)</h3>
                                        <p class="text-so-white text-center">Lorem ipsum dolor sit amet, consectetur </p>
                                    </div>

                                    <a href="/tagfg" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </a>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-6 col-xs-6">
                            <!-- small box -->
                            <a href="/tagfg">
                                <div class="small-box bg-purple card-shadow">
                                    <div class="inner">
                                        <br>
                                        <h3 class="text-center text-so-white" style="font-size:1.5vw;">ต่างประเทศ (SE)</h3>
                                        <p class="text-center text-so-white">Lorem ipsum dolor sit amet, consectetur </p>
                                    </div>

                                    <a href="/tagfg" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

@endsection
